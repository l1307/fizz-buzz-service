package com.leboncoin.fizzbuzz.provider;

import com.generated.fizzbuzz.FizzBuzzSchema;

/**
 * QueueProvider interface.
 *
 * This provider send message in external queue
 */
public interface QueueProvider {

    /**
     * Send a {@link FizzBuzzEntryDTO} in queue.
     *
     * @param fizzBuzzEntryDTO the {@link FizzBuzzEntryDTO} to send in queue
     */
    public void produceMessage(String topic, FizzBuzzSchema fizzBuzzEntryDTO);
    
}
