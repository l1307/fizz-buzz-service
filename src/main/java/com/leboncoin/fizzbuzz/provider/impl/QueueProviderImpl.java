package com.leboncoin.fizzbuzz.provider.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.generated.fizzbuzz.FizzBuzzSchema;
import com.leboncoin.fizzbuzz.provider.QueueProvider;

/**
 * Implementation of the {@link QueueProvider} interface.
 *
 * This provider is transactional and send message in external queue
 */
@Component
@Transactional("kafkaTransactionManager")
public class QueueProviderImpl implements QueueProvider {
    
    /** Logger */
    private static final Logger LOGGER = LogManager.getLogger();
    
    /** Kafka Template injected by Spring (auto-configuration from application.yml) */
    @Autowired
    KafkaTemplate<String, FizzBuzzSchema> kafkaTemplate;

    /**
     * Send a {@link FizzBuzzEntryDTO} in queue.
     *
     * @param fizzBuzzEntryDTO the {@link FizzBuzzEntryDTO} to send in queue
     */
    @Override
    public void produceMessage(String topic, FizzBuzzSchema fizzBuzzEntryDTO) {
        kafkaTemplate.send(topic, fizzBuzzEntryDTO);
        LOGGER.info("FizzBuzzSchema send to queue with following data 'limit: {}', 'int1: {}', 'int2: {}', 'str1: {}', 'str2: {}'", 
                        fizzBuzzEntryDTO.getLimit(),
                        fizzBuzzEntryDTO.getInt1(),
                        fizzBuzzEntryDTO.getInt2(),
                        fizzBuzzEntryDTO.getStr1(),
                        fizzBuzzEntryDTO.getStr2());
    }

}
