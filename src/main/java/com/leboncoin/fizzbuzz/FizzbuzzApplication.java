package com.leboncoin.fizzbuzz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Application initialization class.
 * </p>
 * - All @ {@ link Component} of sub-packages of this class are scanned <br>
 * - Spring configuration files (application.properties | yml | yaml) <br>
 * are loaded <br>
 * - @ {@ link PropertySource} annotation .properties files are loaded <br>
 * - The auto-configuration of the spring boot is triggered
 *
 */
@SpringBootApplication
@EnableTransactionManagement
public class FizzbuzzApplication {

	public static void main(String[] args) {
		SpringApplication.run(FizzbuzzApplication.class, args);
	}

}
