package com.leboncoin.fizzbuzz.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Application output
 *
 */
public class FizzBuzzOutDTO {

    private List<String> fizzBuzzList;

    public List<String> getFizzBuzzList() {
        if (fizzBuzzList == null) {
            fizzBuzzList = new ArrayList<>();
        }
        return fizzBuzzList;
    }

    public void setFizzBuzzList(List<String> fizzBuzzList) {
        this.fizzBuzzList = fizzBuzzList;
    }

}
