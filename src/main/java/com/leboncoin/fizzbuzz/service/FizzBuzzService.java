package com.leboncoin.fizzbuzz.service;

import com.generated.fizzbuzz.FizzBuzzSchema;
import com.leboncoin.fizzbuzz.model.FizzBuzzOutDTO;

/**
 * FizzBuzzService interface.
 *
 * This service validate entry and send message in external queue
 */
public interface FizzBuzzService {

    /**
     * Create a {@link FizzBuzzEntryDTO} and return the {@link FizzBuzzOutDTO}.
     *
     * @param fizzBuzzEntryDTO the {@link FizzBuzzEntryDTO} to send in queue
     * @return the {@link FizzBuzzEntryDTO} created cannot be null.
     */
    public FizzBuzzOutDTO createFizzBuzzList(FizzBuzzSchema fizzBuzzEntryDTO);

    /**
     * Validate a {@link FizzBuzzEntryDTO} content.
     *
     * @param fizzBuzzEntryDTO the {@link FizzBuzzEntryDTO} to be validated
     * @throws {@link ResponseStatusException} with BAD_REQUEST 400 in case of validation error
     */
    public void validateEntry(FizzBuzzSchema fizzBuzzEntryDTO);
}
