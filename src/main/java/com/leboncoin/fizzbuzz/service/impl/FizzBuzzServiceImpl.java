package com.leboncoin.fizzbuzz.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import com.generated.fizzbuzz.FizzBuzzSchema;
import com.leboncoin.fizzbuzz.model.FizzBuzzOutDTO;
import com.leboncoin.fizzbuzz.provider.QueueProvider;
import com.leboncoin.fizzbuzz.service.FizzBuzzService;

/**
 * Implementation of the {@link FizzBuzzService} interface.
 *
 * This service validate entry and send message in external queue
 */
@Component
public class FizzBuzzServiceImpl implements FizzBuzzService {
   
    /** Queue provider injected by Spring */
    @Autowired
    private QueueProvider queueProvider;
    
    /** Topic queue provider injected by Spring from application.yml */
    @Value("${app.topic}")
    private String topic;

    /**
     * Create a {@link FizzBuzzEntryDTO} and return the {@link FizzBuzzOutDTO}.
     *
     * @param fizzBuzzEntryDTO the {@link FizzBuzzEntryDTO} to send in queue
     * @return the {@link FizzBuzzEntryDTO} created cannot be null.
     */
    @Override
    public FizzBuzzOutDTO createFizzBuzzList(FizzBuzzSchema fizzBuzzEntryDTO) {

        final FizzBuzzOutDTO fizzBuzzOutDTO = new FizzBuzzOutDTO();
        final List<String> fizzBuzzList = new ArrayList<>();

        for (int iter = 0; iter < fizzBuzzEntryDTO.getLimit(); iter++) {

            boolean isInMultiple = false;

            final StringBuilder str = new StringBuilder();
            if ((iter + 1) % fizzBuzzEntryDTO.getInt1() == 0) {
               str.append(fizzBuzzEntryDTO.getStr1());
               isInMultiple = true;
            }
            if ((iter + 1) % fizzBuzzEntryDTO.getInt2() == 0) {
                str.append(fizzBuzzEntryDTO.getStr2());
                isInMultiple = true;
             }

            if (isInMultiple) {
                fizzBuzzList.add(str.toString());
                continue;
            }

            fizzBuzzList.add(String.valueOf(iter + 1));
        }

        fizzBuzzOutDTO.setFizzBuzzList(fizzBuzzList);
        
        queueProvider.produceMessage(topic, fizzBuzzEntryDTO);
        
        return fizzBuzzOutDTO;
    }

    /**
     * Validate a {@link FizzBuzzEntryDTO} content.
     *
     * @param fizzBuzzEntryDTO the {@link FizzBuzzEntryDTO} to be validated
     * @throws {@link ResponseStatusException} with BAD_REQUEST 400 in case of validation error
     */
    @Override
    public void validateEntry(FizzBuzzSchema fizzBuzzEntryDTO) {

        // Validate presence
        if (fizzBuzzEntryDTO.getLimit() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "limit field is mandatory");
        }
        if (fizzBuzzEntryDTO.getStr1() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "str1 field is mandatory");
        }
        if (fizzBuzzEntryDTO.getStr2() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "str2 field is mandatory");
        }
        if (fizzBuzzEntryDTO.getInt1() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "int1 field is mandatory");
        }
        if (fizzBuzzEntryDTO.getInt2() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "int2 field is mandatory");
        }
        
        
        // Validate sizes
        if (fizzBuzzEntryDTO.getStr1().length() > 32) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "str1 field is too long, must be 32 digits max");
        }
        if (fizzBuzzEntryDTO.getStr2().length() > 32) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "str2 field is too long, must be 32 digits max");
        }
        if (fizzBuzzEntryDTO.getInt1() > 1000) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "int1 field is too high, must be 1000 max");
        }
        if (fizzBuzzEntryDTO.getInt2() > 1000) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "int2 field is too high, must be 1000 max");
        }
        if (fizzBuzzEntryDTO.getLimit() > 1000) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "limit field is too high, must be 1000 max");
        }
        
    }

}
