package com.leboncoin.fizzbuzz.security.interceptor;

import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

/**
 * JwtInterceptor: intercept token and control it thanks to public key
 */
@Component
public class JwtInterceptor implements HandlerInterceptor, InitializingBean {
    
    @Value("${app.public-key}")
    private String stringPublicKey;
    
    public PublicKey publicKey;

    /**
     * Pre handle: verify token
     *
     * @param httpServletRequest the http servlet request
     * @param httpServletResponse the http servlet response
     * @param object the object
     * @return true, if successful
     * @throws ResponseStatusException the 401 error
     */
    @Override
    public boolean preHandle(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse, final Object object) throws Exception {

        DecodedJWT token = null;
        try {
            token = JWT.decode(httpServletRequest.getHeader("Authorization").replace("Bearer ", ""));

            final Algorithm algorithm = Algorithm.RSA256((RSAPublicKey) publicKey, null);
            final JWTVerifier verifier = JWT.require(algorithm).build();
            verifier.verify(token);
        } catch(final Exception ex) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid JWT Token");
        }

        return true;
    }

    /**
     * Post handle.
     *
     * @param httpServletRequest the http servlet request
     * @param httpServletResponse the http servlet response
     * @param o the o
     * @param modelAndView the model and view
     * @throws Exception the exception
     */
    @Override
    public void postHandle(final HttpServletRequest httpServletRequest,
            final HttpServletResponse httpServletResponse,
            final Object o, final ModelAndView modelAndView) throws Exception {

    }

    /**
     * After completion.
     *
     * @param httpServletRequest the http servlet request
     * @param httpServletResponse the http servlet response
     * @param o the o
     * @param e the e
     * @throws Exception the exception
     */
    @Override
    public void afterCompletion(final HttpServletRequest httpServletRequest,
            final HttpServletResponse httpServletResponse,
            final Object o, final Exception e) throws Exception {
    }

    @Override
    public void afterPropertiesSet() throws Exception {

        final KeyFactory kf = KeyFactory.getInstance("RSA");
        final X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(Base64.getDecoder().decode(stringPublicKey));

        this.publicKey = kf.generatePublic(keySpecX509);
        
    }

}

