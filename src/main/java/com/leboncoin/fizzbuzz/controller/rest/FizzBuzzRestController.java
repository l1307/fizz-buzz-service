package com.leboncoin.fizzbuzz.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.generated.fizzbuzz.FizzBuzzSchema;
import com.leboncoin.fizzbuzz.model.FizzBuzzOutDTO;
import com.leboncoin.fizzbuzz.service.FizzBuzzService;

/**
 * REST fizzbuzz management controller.
 * <p>
 * - we avoid triggering validations in the controller (unless they are specific to the REST layer), but we trigger them instead
 * in the service layer in order to be shared regardless of the controller. <br>
 * - for the methods for which we want to be able to return headers (Location for example), or return different responses in
 * depending on the feedback from the service layer, we use a return type {@link ResponseEntity} allowing to control more finely the
 * HTTP response. <br>
 * - to return errors / problems to the customer containing a body with the details of the problem, we will privilege exceptional reports
 * of the type {@link ResponseStatusException} with status code for errors
 *
 */
@RestController
@RequestMapping(path = "/")
public class FizzBuzzRestController {

    /** Business service injected by Spring */
    @Autowired
    private FizzBuzzService fizzBuzzService;
    
    /**
     * REST POST request to create a Resource {@link FizzBuzzEntryDTO}.
     *
     * @param fizzBuzzEntryDTO the {@link FizzBuzzEntryDTO} to create. This entity will be sent to a queue
     * @return the created resource is wrapped in a {@link FizzBuzzOutDTO} resource and a 200 code
     */
    @PostMapping
    public ResponseEntity<FizzBuzzOutDTO> create(@RequestBody FizzBuzzSchema fizzBuzzEntryDTO) {

        fizzBuzzService.validateEntry(fizzBuzzEntryDTO);

        return ResponseEntity.ok(fizzBuzzService.createFizzBuzzList(fizzBuzzEntryDTO));
    }

}
