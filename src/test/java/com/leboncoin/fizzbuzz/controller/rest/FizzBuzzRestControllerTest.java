package com.leboncoin.fizzbuzz.controller.rest;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.server.ResponseStatusException;
import com.generated.fizzbuzz.FizzBuzzSchema;
import com.leboncoin.fizzbuzz.model.FizzBuzzOutDTO;
import com.leboncoin.fizzbuzz.service.FizzBuzzService;
import com.leboncoin.fizzbuzz.service.impl.FizzBuzzServiceImpl;
import com.leboncoin.fizzbuzz.spy.QueueProviderSpy;

/**
 * This class test {@link FizzBuzzRestController} and all relative services and provider
 * 
 * To ensure that application respect business rules, all test are based on rules defined on business rules inside doc folder
 *
 */
public class FizzBuzzRestControllerTest {

    private FizzBuzzRestController fizzBuzzRestController;
    
    private FizzBuzzService fizzBuzzService;
    
    private QueueProviderSpy queueProvider;
    
    private String topic;

    @BeforeEach
    public void setUp() {
        fizzBuzzRestController = new FizzBuzzRestController();
        fizzBuzzService = new FizzBuzzServiceImpl();
        queueProvider = new QueueProviderSpy();
        topic = "spyTopic";
        
        ReflectionTestUtils.setField(fizzBuzzService, "topic", topic);
        ReflectionTestUtils.setField(fizzBuzzService, "queueProvider", queueProvider);
        ReflectionTestUtils.setField(fizzBuzzRestController, "fizzBuzzService", fizzBuzzService);
    }

    /**
     * RULE_API_CONTRACT for field limit
     */
    @Test
    public void validate_mandatory_limit_field() {

        // Prepare
        final FizzBuzzSchema fizzBuzzSchema = createFizzBuzzSchema(null, 1, 2, "fizz", "buzz");

        // Act
        Assertions.assertThatThrownBy(() -> {
            fizzBuzzRestController.create(fizzBuzzSchema);
        }).isInstanceOf(ResponseStatusException.class)
                        .extracting(e -> ((ResponseStatusException) e))
                        .satisfies(e -> HttpStatus.BAD_REQUEST.equals(e.getStatus()))
                        .satisfies(e -> "limit field is mandatory".equals(e.getReason()));

        // Verify
        Assertions.assertThat(fizzBuzzSchema);
        Assertions.assertThat(queueProvider.pollTopic(topic)).isNull();
    }

    /**
     * RULE_API_CONTRACT for field int1
     */
    @Test
    public void validate_mandatory_int1_field() {

        // Prepare
        final FizzBuzzSchema fizzBuzzSchema = createFizzBuzzSchema(10, null, 2, "fizz", "buzz");

        // Act
        Assertions.assertThatThrownBy(() -> {
            fizzBuzzRestController.create(fizzBuzzSchema);
        }).isInstanceOf(ResponseStatusException.class)
                        .extracting(e -> ((ResponseStatusException) e))
                        .satisfies(e -> HttpStatus.BAD_REQUEST.equals(e.getStatus()))
                        .satisfies(e -> "int1 field is mandatory".equals(e.getReason()));

        // Verify
        Assertions.assertThat(fizzBuzzSchema);
        Assertions.assertThat(queueProvider.pollTopic(topic)).isNull();
    }
    
    /**
     * RULE_API_CONTRACT for field int2
     */
    @Test
    public void validate_mandatory_int2_field() {

        // Prepare
        final FizzBuzzSchema fizzBuzzSchema = createFizzBuzzSchema(10, 1, null, "fizz", "buzz");

        // Act
        Assertions.assertThatThrownBy(() -> {
            fizzBuzzRestController.create(fizzBuzzSchema);
        }).isInstanceOf(ResponseStatusException.class)
                        .extracting(e -> ((ResponseStatusException) e))
                        .satisfies(e -> HttpStatus.BAD_REQUEST.equals(e.getStatus()))
                        .satisfies(e -> "int2 field is mandatory".equals(e.getReason()));

        // Verify
        Assertions.assertThat(fizzBuzzSchema);
        Assertions.assertThat(queueProvider.pollTopic(topic)).isNull();
    }
    
    /**
     * RULE_API_CONTRACT for field str1
     */
    @Test
    public void validate_mandatory_str1_field() {

        // Prepare
        final FizzBuzzSchema fizzBuzzSchema = createFizzBuzzSchema(10, 1, 2, null, "buzz");

        // Act
        Assertions.assertThatThrownBy(() -> {
            fizzBuzzRestController.create(fizzBuzzSchema);
        }).isInstanceOf(ResponseStatusException.class)
                        .extracting(e -> ((ResponseStatusException) e))
                        .satisfies(e -> HttpStatus.BAD_REQUEST.equals(e.getStatus()))
                        .satisfies(e -> "str1 field is mandatory".equals(e.getReason()));

        // Verify
        Assertions.assertThat(fizzBuzzSchema);
        Assertions.assertThat(queueProvider.pollTopic(topic)).isNull();
    }
    
    /**
     * RULE_API_CONTRACT for field str2
     */
    @Test
    public void validate_mandatory_str2_field() {

        // Prepare
        final FizzBuzzSchema fizzBuzzSchema = createFizzBuzzSchema(10, 1, 2, "fizz", null);

        // Act
        Assertions.assertThatThrownBy(() -> {
            fizzBuzzRestController.create(fizzBuzzSchema);
        }).isInstanceOf(ResponseStatusException.class)
                        .extracting(e -> ((ResponseStatusException) e))
                        .satisfies(e -> HttpStatus.BAD_REQUEST.equals(e.getStatus()))
                        .satisfies(e -> "str2 field is mandatory".equals(e.getReason()));

        // Verify
        Assertions.assertThat(queueProvider.pollTopic(topic)).isNull();
    }

    /**
     * RULE_API_CONTRACT for field limit size
     */
    @Test
    public void validate_size_limit_field() {

        // Prepare
        final FizzBuzzSchema fizzBuzzSchema = createFizzBuzzSchema(1001, 1, 2, "fizz", "buzz");

        // Act
        Assertions.assertThatThrownBy(() -> {
            fizzBuzzRestController.create(fizzBuzzSchema);
        }).isInstanceOf(ResponseStatusException.class)
                        .extracting(e -> ((ResponseStatusException) e))
                        .satisfies(e -> HttpStatus.BAD_REQUEST.equals(e.getStatus()))
                        .satisfies(e -> "limit field is too high, must be 1000 max".equals(e.getReason()));

        // Verify
        Assertions.assertThat(fizzBuzzSchema);
        Assertions.assertThat(queueProvider.pollTopic(topic)).isNull();
    }

    /**
     * RULE_API_CONTRACT for field int1 size
     */
    @Test
    public void validate_size_int1_field() {

        // Prepare
        final FizzBuzzSchema fizzBuzzSchema = createFizzBuzzSchema(10, 1001, 2, "fizz", "buzz");

        // Act
        Assertions.assertThatThrownBy(() -> {
            fizzBuzzRestController.create(fizzBuzzSchema);
        }).isInstanceOf(ResponseStatusException.class)
                        .extracting(e -> ((ResponseStatusException) e))
                        .satisfies(e -> HttpStatus.BAD_REQUEST.equals(e.getStatus()))
                        .satisfies(e -> "int1 field is too high, must be 1000 max".equals(e.getReason()));

        // Verify
        Assertions.assertThat(fizzBuzzSchema);
        Assertions.assertThat(queueProvider.pollTopic(topic)).isNull();
    }
    
    /**
     * RULE_API_CONTRACT for field int2 size
     */
    @Test
    public void validate_size_int2_field() {

        // Prepare
        final FizzBuzzSchema fizzBuzzSchema = createFizzBuzzSchema(10, 1, 1001, "fizz", "buzz");

        // Act
        Assertions.assertThatThrownBy(() -> {
            fizzBuzzRestController.create(fizzBuzzSchema);
        }).isInstanceOf(ResponseStatusException.class)
                        .extracting(e -> ((ResponseStatusException) e))
                        .satisfies(e -> HttpStatus.BAD_REQUEST.equals(e.getStatus()))
                        .satisfies(e -> "int2 field is too high, must be 1000 max".equals(e.getReason()));

        // Verify
        Assertions.assertThat(fizzBuzzSchema);
        Assertions.assertThat(queueProvider.pollTopic(topic)).isNull();
    }
    
    /**
     * RULE_API_CONTRACT for field str1 size
     */
    @Test
    public void validate_size_str1_field() {

        // Prepare
        final FizzBuzzSchema fizzBuzzSchema = createFizzBuzzSchema(10, 1, 2, "123456789123456789123456789123456789", "buzz");

        // Act
        Assertions.assertThatThrownBy(() -> {
            fizzBuzzRestController.create(fizzBuzzSchema);
        }).isInstanceOf(ResponseStatusException.class)
                        .extracting(e -> ((ResponseStatusException) e))
                        .satisfies(e -> HttpStatus.BAD_REQUEST.equals(e.getStatus()))
                        .satisfies(e -> "str1 field is too long, must be 32 digits max".equals(e.getReason()));

        // Verify
        Assertions.assertThat(fizzBuzzSchema);
        Assertions.assertThat(queueProvider.pollTopic(topic)).isNull();
    }
    
    /**
     * RULE_API_CONTRACT for field str2 size
     */
    @Test
    public void validate_size_str2_field() {

        // Prepare
        final FizzBuzzSchema fizzBuzzSchema = createFizzBuzzSchema(10, 1, 2, "fizz", "123456789123456789123456789123456789");

        // Act
        Assertions.assertThatThrownBy(() -> {
            fizzBuzzRestController.create(fizzBuzzSchema);
        }).isInstanceOf(ResponseStatusException.class)
                        .extracting(e -> ((ResponseStatusException) e))
                        .satisfies(e -> HttpStatus.BAD_REQUEST.equals(e.getStatus()))
                        .satisfies(e -> "str2 field is too long, must be 32 digits max".equals(e.getReason()));

        // Verify
        Assertions.assertThat(queueProvider.pollTopic(topic)).isNull();
    }

    /**
     * RULE_API_RESULT_SIZE check that output arraysize corresponding to "limit" parameter
     */
    @Test
    public void post_a_limit_of_100_and_expect_an_array_size_of_100() {

        // Prepare
        final FizzBuzzSchema fizzBuzzSchema = createFizzBuzzSchema(100, 1, 2, "fizz", "buzz");

        // Act
        ResponseEntity<FizzBuzzOutDTO> response = fizzBuzzRestController.create(fizzBuzzSchema);

        // Verify
        Assertions.assertThat(response.getBody().getFizzBuzzList()).hasSize(100);
        Assertions.assertThat(queueProvider.pollTopic(topic))
            .isNotNull()
            .isEqualTo(fizzBuzzSchema);
    }

    /**
     * RULE_API_LIST_FILLING_GLOBAL All entities values are entity's position in this list exept for multiples to append
     * RULE_API_LIST_FILLING_APPEND_PAIRS Verify all entities with multiples to append
     */
    @Test
    public void post_a_limit_of_16_and_verify_multiples_to_append_are_added() {

        // Prepare
        final FizzBuzzSchema fizzBuzzSchema = createFizzBuzzSchema(16, 3, 5, "fizz", "buzz");

        // Act
        ResponseEntity<FizzBuzzOutDTO> response = fizzBuzzRestController.create(fizzBuzzSchema);

        // Verify
        Assertions.assertThat(response.getBody().getFizzBuzzList())
            .hasSize(16)
            .satisfies(l -> l.get(0).equals("1"))
            .satisfies(l -> l.get(1).equals("2"))
            .satisfies(l -> l.get(2).equals("fizz"))
            .satisfies(l -> l.get(3).equals("4"))
            .satisfies(l -> l.get(4).equals("buzz"))
            .satisfies(l -> l.get(5).equals("fizz"))
            .satisfies(l -> l.get(6).equals("7"))
            .satisfies(l -> l.get(7).equals("8"))
            .satisfies(l -> l.get(8).equals("fizz"))
            .satisfies(l -> l.get(9).equals("buzz"))
            .satisfies(l -> l.get(10).equals("11"))
            .satisfies(l -> l.get(11).equals("fizz"))
            .satisfies(l -> l.get(12).equals("13"))
            .satisfies(l -> l.get(13).equals("14"))
            .satisfies(l -> l.get(14).equals("fizzbuzz"))
            .satisfies(l -> l.get(15).equals("16"));
        
        Assertions.assertThat(queueProvider.pollTopic(topic))
            .isNotNull()
            .isEqualTo(fizzBuzzSchema);
                        
    }

    private FizzBuzzSchema createFizzBuzzSchema(Integer limit, Integer int1, Integer int2, String str1, String str2) {
        final FizzBuzzSchema fizzBuzzSchema = new FizzBuzzSchema();
        fizzBuzzSchema.setLimit(limit);
        fizzBuzzSchema.setInt1(int1);
        fizzBuzzSchema.setInt2(int2);
        fizzBuzzSchema.setStr1(str1);
        fizzBuzzSchema.setStr2(str2);
        return fizzBuzzSchema;
    }

}
