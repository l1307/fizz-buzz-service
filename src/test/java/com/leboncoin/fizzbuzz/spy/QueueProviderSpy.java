package com.leboncoin.fizzbuzz.spy;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import com.generated.fizzbuzz.FizzBuzzSchema;
import com.leboncoin.fizzbuzz.provider.QueueProvider;

/**
 * This spy mock a queue provider like Kafka/JMS/MQ
 * 
 * It allow junit tests to not depends of the chosen queue system
 *
 */
public class QueueProviderSpy implements QueueProvider {

    /** Persistence queue map */
    private final Map<String, List<FizzBuzzSchema>> recordMap = new HashMap<>();
    
    /**
     * Produce a message {@link FizzBuzzSchema} on persistence queue map.
     *
     * @param topic is queue topic
     * @param fizzBuzzSchema the {@link FizzBuzzSchema} to send in queue
     */
    @Override
    public void produceMessage(String topic, FizzBuzzSchema fizzBuzzSchema) {
        if(!this.recordMap.containsKey(topic)) {
            this.recordMap.put(topic, new LinkedList<>());
        }
        this.recordMap.get(topic).add(fizzBuzzSchema);
    }
    
    /**
     * Retreive a {@link FizzBuzzSchema} from queue topic.
     *
     * @param topic is queue topic
     * @return the first {@link FizzBuzzSchema} in queue.
     */
    public FizzBuzzSchema pollTopic(String topic) {
        if(this.recordMap.containsKey(topic)) {
            List<FizzBuzzSchema> records = this.recordMap.get(topic);
            FizzBuzzSchema record = records.get(0);
            records.remove(0);

            return record;
        }
        return null;
    }

}
