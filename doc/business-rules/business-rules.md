# Business Rules

This documentation refers to all business Rules of the **Simple fizz-buzz REST server**.

## Business rules

### RULE_API_CONTRACT
All following parameters of **Simple fizz-buzz REST server** are mandatory:
 * **limit** (int limited to 1000) 
 * **int1**  (int limited to 1000)
 * **int2**  (int limited to 1000)
 * **str1** (string limited size to 32)
 * **str2** (string limited size to 32)

### RULE_API_RESULT_SIZE
**Simple fizz-buzz REST server** return a list of strings, the size of this list corresponding to **limit** parameter.

### RULE_API_LIST_FILLING_GLOBAL
All entities inside **Simple fizz-buzz REST server** returned array are entity's position in this list.
This rule doesn’t apply for multiple of **int1** or **int2**.

### RULE_API_LIST_FILLING_INT1_MULTIPLE
Append **str1** value for all entities inside **Simple fizz-buzz REST server** returned array which is multiple of **int1**

### RULE_API_LIST_FILLING_INT2_MULTIPLE
Append **str2** value for all entities inside **Simple fizz-buzz REST server** returned array which is multiple of **int2**