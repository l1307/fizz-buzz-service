# Application configuration variables

This documentation refers to all dynamic configurations of **Simple fizz-buzz REST server**.

| Variable  | Description  | Default value |
|---|---|---|
| KAKFA_BROKER | Kafka broker hostname  | localhost:9093  |
| SCHEMA_REGISTRY | Kafka schema registry adress  | "http://localhost:8081" |
| KAFKA_FIZZ_BUZZ_TOPIC | Kafka output topic name | fizzbuzztopic |
| PUBLIC_KEY | Public key to decode JWT tokens| Check on application.yml|